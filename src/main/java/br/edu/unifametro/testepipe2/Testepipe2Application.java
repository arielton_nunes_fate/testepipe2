package br.edu.unifametro.testepipe2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Testepipe2Application {

	public static void main(String[] args) {
		SpringApplication.run(Testepipe2Application.class, args);
	}
	
	@RequestMapping("/")
	public String home() {
		return "Deu certo professor, parece o curso do Gustavo Guanabara";
	}

}
